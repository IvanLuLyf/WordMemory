# WordMemory

一个背单词的网页

![GitHub](https://img.shields.io/github/license/ivanlulyf/wordmemory.svg?color=blue)

创意来自[ZE3kr/MemWords-CN](https://github.com/ZE3kr/MemWords-CN.git)

体验地址[点这里](https://ivanlulyf.github.io/WordMemory/index.html)
